// ****************************************************************
// KEB S6(c) Drive interface with motion profile calculation on the drive (DS402 Mode of Operation 2)
//
// DISCLAIMER and MIT Copyright (c) [2021] [Oxni GmbH]
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// 
// Version	Date		Who			What
// ****************************************************************
// V 1.000	28.07.21	MRupf Oxni	First version
// ****************************************************************
#pragma once

//*** Required header files ***
#include <windows.h>
#include <stdint.h>
#include "c:\sha\globdef64.h"
#include "c:\sha\Sha64Debug.h"
#include "c:\eth\Sha64EthCore.h"
#include "c:\eth\Eth64CoreDef.h"
#include "c:\eth\Eth64Macros.h"
#include "c:\ect\Sha64EcatCore.h"
#include "c:\ect\Ecat64CoreDef.h"
#include "c:\ect\Ecat64Macros.h"
#include "c:\ect\Ecat64Control.h"
#include "c:\ect\Ecat64SilentDef.h"


//*** Structures need to have 1 byte alignment ***
#pragma pack(push, old_alignment)
#pragma pack(1)

//*** TX mapping structure [Device Output] ***
struct TX_MAP_S6A_v1
{
	uint16_t statusWord;	// 2 Byte status word h6041
	uint32_t  actPos;		// 4 Byte actual position h6064
	uint32_t  actVel;		// 4 Byte actual velocity h606c (ru08 * co02 for higher resolution)
	uint8_t  opMode;		// 1 Byte actual mode of operation h6061
	uint16_t actVel2;		// 2 Byte actual velocity h6044 (ru08 only integer rpm)
	uint32_t actCur;		// 4 Byte actual current h2c0a
	uint16_t digitalInput;	// 2 Byte digital Input status h2c12 (ru18)
};

//*** RX mapping structure [Device Input]  ***
struct RX_MAP_S6A_v1
{
	uint16_t controlWord;	// 2 Byte control word h6040
	uint8_t  opMode;		// 1 Byte mode of operation h6060
	uint16_t cmdVelPV;		// 2 Byte velocity command h6042 (vl.20)
	uint32_t cmdVelPV2;		// 4 Byte high resolution velocity command h2315 (vl.21)
	uint16_t digitalOutput;	// 2 Byte digital Output control h260a (do10)
};


//Set back old alignment
#pragma pack(pop, old_alignment)

// S6 Class
class S6ProfileVelocity {
private:
	bool _enable = false;				// Enable/Disable Drive
	bool _isEnabled = false;
	bool _doReset = false;				// Enable routine will include reset in next call
	bool _error = false;				// If any action fails
	bool _moveJog = false;				// Start jog at constant velocity
	bool _stop = false;					// Dont' allow any motion
	int32_t _cmdVel = 0;
	int32_t _cmdVelHiRes = 0;			// Will be added to _cmdVel with a 1/8192rpm resolution
	int32_t _cmdAcc = 0;
	int32_t _cmdDec = 0;
	uint8_t _actMode = 0;
	double _posScaling;					// User Units per revolution
	uint32_t _digitalOutput = 0;			// output are set zero by default
	int64_t _internalPos = 0;			// Internal position upscaled to 64 bit from 32 bit EtherCat
	int64_t _internalPosOverflow = 0;	// INternal position overflow counter
	const TX_MAP_S6A_v1* txMap;
	RX_MAP_S6A_v1* rxMap;

public:
	// SW Version
	static const uint32_t SW_VERSION = 0x0001;

	// Constructor
	S6ProfileVelocity(double posScaling);

	// Return drive fault
	bool getDriveFault();

	// Return drive warning
	bool getDriveWarning();

	// Return error status
	bool getErrorStatus();

	// Return enable status
	bool getEnableStatus();

	// Return status as bit combined int
	uint32_t getDriveStatus();

	// Get Actual Position as float in User Units
	double getActPosUU();

	// Get Actual Speed in RPM
	double getActVel();

	// Get Actual Current
	double getActCurrent();

	// Get digital input and output status
	uint32_t getDigInStat();

	// REALTIME TASK this routine has to be called in each cycle
	// Mapping of all commands to and from the EtherCat
	void realTimeTask(const TX_MAP_S6A_v1* txMap, RX_MAP_S6A_v1* rxMap);

	// Enable/Disable procedure
	void enableDrive(bool);

	// Reset drive faults
	void resetFault();

	// Jog inputs scaled in UU
	void moveVel(double moveVel);

	// Set digital output  (outIdx 1 or 2 are only valid)
	void setDigOut(int outIdx, bool value);

	// SDO control to the drive
	static uint64_t sendSDOCmd(PSTATION_INFO pStation, bool iWrite, USHORT idx, UCHAR subIdx, ULONG length, PUCHAR data);

	// SDO download request
	static uint64_t sdoControl_S6(PSTATION_INFO);
};


