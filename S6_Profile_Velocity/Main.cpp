//***************************************
//  Automated generated code template ***
//  EtherCAT Code Type : [Expert DC]  ***
//***************************************

// *****************************************************************
// Main Routine, splitted into Realtime- and Non- Realtime tasks
// 
// DISCLAIMER and MIT Copyright (c) [2021] [Oxni GmbH]
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// 
// Version	Date		Who			What
// *****************************************************************
// V 0.001	13.07.21	MRupf Oxni	First version
// *****************************************************************

#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include "c:\sha\globdef64.h"
#include "c:\sha\sha64debug.h"
#include "c:\eth\Sha64EthCore.h"
#include "c:\eth\Eth64CoreDef.h"
#include "c:\eth\Eth64Macros.h"
#include "c:\ect\Sha64EcatCore.h"
#include "c:\ect\Ecat64CoreDef.h"
#include "c:\ect\Ecat64Macros.h"
#include "c:\ect\Ecat64Control.h"
#include "c:\ect\Ecat64SilentDef.h"

//Configuration Header Includes
#include "DEFINES.h"
#include "S6_PV.h"

#pragma comment( lib, "c:\\sha\\sha64dll.lib" )
#pragma comment( lib, "c:\\eth\\sha64ethcore.lib" )
#pragma comment( lib, "c:\\ect\\sha64ecatcore.lib" )

#pragma warning(disable:4996)
#pragma warning(disable:4748)

//************************************
//#define SEQ_DEBUG			1
//************************************

//Declare global elements
PETH_STACK		__pUserStack = NULL;		//Ethernet Core Stack (used outside Realtime Task)
PETH_STACK		__pSystemStack = NULL;		//Ethernet Core Stack (used inside Realtime Task)
PSTATION_INFO	__pUserList = NULL;			//Station List (used outside Realtime Task)
PSTATION_INFO	__pSystemList = NULL;		//Station List (used inside Realtime Task)
USHORT			__StationNum = 0;			//Number of Stations
FP_ECAT_ENTER	__fpEcatEnter = NULL;		//Function pointer to Wrapper EcatEnter
FP_ECAT_EXIT	__fpEcatExit = NULL;		//Function pointer to Wrapper EcatExit
ULONG			__EcatState = 0;			//Initial Wrapper State
ULONG			__UpdateCnt = 0;			//Station Update Counter
ULONG			__LoopCnt = 0;				//Realtime Cycle Counter
ULONG			__ReadyCnt = 0;				//Ready state counter
STATE_OBJECT    __StateObject = { 0 };		//Cyclic state object
BOOLEAN			__bLogicReady = FALSE;		//Logic Ready Flag

// Some variables to play with
int64_t g_sdoData = 0;
int32_t g_CmdWord = 0;
S6ProfileVelocity g_S6_1(360.0);	// One S6 Axis with 360.0 User Units per revolution
uint64_t g_currentTime = 0;			// run time counter in [ms]

//***************************
#ifdef SEQ_DEBUG
	SEQ_INIT_ELEMENTS(); //Init sequencing elements
#endif
//***************************


__inline PVOID __GetMapDataByName(char* szName, int Index, bool bInput)
{
	int cnt = 0;

	//Loop through station list
	for (USHORT i = 0; i < __StationNum; i++)
		if (strstr(__pUserList[i].szName, szName))
			if (cnt++ == Index)
			{
				//Get station pointer and return index
				if (bInput) { return __pUserList[i].RxTel.s.data; }
				else		{ return __pUserList[i].TxTel.s.data; }
			}

	//Station not found
	return NULL;
}


void static DoLogic_ProfilVelocity()
{
	// Get run time of software in [ms]
	g_currentTime += (REALTIME_PERIOD * SYNC_CYCLES) / 1000;

	//Get mapped data pointers
	g_S6_1.realTimeTask((TX_MAP_S6A_v1*)__GetMapDataByName((char*)"S6A", 0, TRUE),    //1. Device
		(RX_MAP_S6A_v1*)__GetMapDataByName((char*)"S6A", 0, false));   //1. Device
	//ToDo: Payload logic
	//...

//****************************
#ifdef SEQ_DEBUG
	SYSTEM_SEQ_CAPTURE("SEQ", __LINE__, 0, TRUE);
#endif
//****************************
}


void static AppTask(void* param)
{
	//Call enter wrapper function
	__EcatState = __fpEcatEnter(
							__pSystemStack,
							__pSystemList,
							(USHORT)__StationNum,
							&__StateObject);


	//Check operation state and increase ready count
	if (__EcatState == ECAT_STATE_READY) { __ReadyCnt--; }
	else                                 { __ReadyCnt = SYNC_CYCLES; }

	//Check ready count
	if (__ReadyCnt == 1)
	{
		//**********************************
		if (__bLogicReady) { 
			DoLogic_ProfilVelocity(); 
		}
		//**********************************

		//Update counter
		__UpdateCnt++;
	}

	//Call exit function
	__fpEcatExit();
	
	//Increase loop count
	__LoopCnt++;
}

void doRun_ProfileVelocity() {

	// *** Execute commands form HMI ***
	// Bit 0 = enable
	if (g_CmdWord & 1) {
		g_S6_1.enableDrive(!g_S6_1.getEnableStatus());
		// Reset bit command
		g_CmdWord = g_CmdWord & 0xFFFE;
	}
	// Bit 1 = stop
	if (g_CmdWord & 2 ) {
		g_S6_1.moveVel(0.0);
		// Reset bit command
		g_CmdWord = g_CmdWord & 0xFFFD;
	}
	// Bit 2 = reset fault
	if (g_CmdWord & 4) {
		g_S6_1.resetFault();
		// Reset bit command
		g_CmdWord = g_CmdWord & 0xFFFB;
	}
	// Jog
	if (g_CmdWord & 8) {
		g_S6_1.moveVel(600);
		g_CmdWord = g_CmdWord & 0xFFF7;
	}
	
	// Write Acc
	if (g_CmdWord & 16) {
		g_sdoData = (int64_t)(100.00 * 100.0);
		g_S6_1.sendSDOCmd(&__pUserList[0], true, 0x2530, 0, 4, (PUCHAR)&g_sdoData);
	}
	// Write Dec
	if (g_CmdWord & 32) {
		g_sdoData = (int64_t)(100.00 * 100.0);
		g_S6_1.sendSDOCmd(&__pUserList[0], true, 0x2531, 0, 4, (PUCHAR)&g_sdoData);
	}

	// Bit 7 = write sdo
	if (g_CmdWord & 128) {
		g_sdoData = 1000;
		g_S6_1.sendSDOCmd(&__pUserList[0], true, 0x2530, 0, 4, (PUCHAR)&g_sdoData);
		// Reset bit command
		g_CmdWord = g_CmdWord & 0xFF7F;
	}
	// Bit 8 = read sdo
	if (g_CmdWord & 256) {
		g_sdoData = g_S6_1.sendSDOCmd(&__pUserList[0], false, 0x2530, 0, 4, (PUCHAR)&g_sdoData);
		// Reset bit command
		g_CmdWord = g_CmdWord & 0xFEFF;
	}

	//Display TX and RX information
	//printf("Update Count: %i\r", __UpdateCnt);
	if (g_S6_1.getDriveFault()) {
		printf("Drive Fault\r");
	}
	else {
		printf("Actual velocity [rpm]: %f\r", g_S6_1.getActVel());
	}
}

// Service Data exchange
__inline ULONG __SdoControl(void)
{
	//Loop through station list
	for (USHORT i=0; i<__StationNum;i++)
	{
		 PSTATION_INFO pStation = (PSTATION_INFO)&__pUserList[i];

		 //Do Device SDO control
		 if (strstr(pStation->szName, "S6series")) {
			 if (g_S6_1.sdoControl_S6(pStation) != -1) {
				 return (-1);
			 }
		 }
	}

	//Do default PDO assignment
	return Ecat64PdoAssignment();
	//return 0;
}

// Main routine
int main(void)
{
//******************
#ifdef SEQ_DEBUG
	SEQ_ATTACH();						//Attach to sequence memory
	SEQ_RESET(TRUE, FALSE, NULL, 0);	//Reset/Init sequence memory
#endif
//******************

	printf("\n*** EtherCAT Code Type: [Expert DC] ***\n\n");

	//Set ethernet mode
	__EcatSetEthernetMode(0, REALTIME_PERIOD, SYNC_CYCLES, TRUE);

	//Required ECAT parameters
	ECAT_PARAMS EcatParams;
	memset(&EcatParams, 0, sizeof(ECAT_PARAMS));
	EcatParams.PhysAddr = DEFAULT_PHYSICAL_ADDRESS;
	EcatParams.LogicalAddr = DEFAULT_LOGICAL_ADDRESS;
	EcatParams.SyncCycles = SYNC_CYCLES;			//Set cycles for synchronisation interval
	EcatParams.EthParams.dev_num = 0;				//Set NIC index [0..7]
	EcatParams.EthParams.period = REALTIME_PERIOD;	//Set realtime period [�sec]
	EcatParams.EthParams.fpAppTask = AppTask;

	//******************************
	//Create ECAT realtime core
	//******************************
	if (ERROR_SUCCESS == Sha64EcatCreate(&EcatParams))
	{
		Sleep(1000);

		//Init global elements
		__pUserStack	= EcatParams.EthParams.pUserStack;
		__pSystemStack	= EcatParams.EthParams.pSystemStack;
		__pUserList		= EcatParams.pUserList;
		__pSystemList	= EcatParams.pSystemList;
		__StationNum	= EcatParams.StationNum;
		__fpEcatEnter	= EcatParams.fpEcatEnter;
		__fpEcatExit	= EcatParams.fpEcatExit;

		//Display version information
		Sha64EcatGetVersion(&EcatParams);
		printf("ECTCORE-DLL : %.2f\nECTCORE-DRV : %.2f\n", EcatParams.core_dll_ver           / (double)100, EcatParams.core_drv_ver / (double)100);
		printf("ETHCORE-DLL : %.2f\nETHCORE-DRV : %.2f\n", EcatParams.EthParams.core_dll_ver / (double)100, EcatParams.EthParams.core_drv_ver / (double)100);
		printf("SHA-LIB     : %.2f\nSHA-DRV     : %.2f\n", EcatParams.EthParams.sha_lib_ver  / (double)100, EcatParams.EthParams.sha_drv_ver / (double)100);
		printf("\n");

		//Display station information
		for (int i = 0; i < __StationNum; i++) {
			printf("Station: %d\n", (ULONG)i);
			if (strstr(__pUserList[i].szName, "S6series")) {
				printf("S6A %d\n", g_S6_1.SW_VERSION);
			}
			else {
				printf(__pUserList[i].szName);
			}
		}

		//******************************
		//Enable Stations
		//******************************

		//Change state to INIT
		if (ERROR_SUCCESS == Ecat64ChangeAllStates(AL_STATE_INIT))
		{
			UCHAR Data[0x100] = { 0 };

			//Reset devices
			Ecat64ResetDevices();
			Ecat64SendCommand(BWR_CMD, 0x0000,  0x300, 8, Data);

			//Set fixed station addresses and
			//Init FMMUs and SYNCMANs
			if (ERROR_SUCCESS == Ecat64InitStationAddresses(EcatParams.PhysAddr))
			if (ERROR_SUCCESS == Ecat64InitFmmus(EcatParams.LogicalAddr))
			if (ERROR_SUCCESS == Ecat64InitSyncManagers())
			{
				//Change state to PRE OPERATIONAL
				//Init PDO assignment
				if (ERROR_SUCCESS == Ecat64ChangeAllStates(AL_STATE_PRE_OP))
				if (ERROR_SUCCESS == __SdoControl())
				{
					//Drift compensation delay [msec]
					ULONG CompLoops = 1000;

					//Init DC immediately after cyclic operation has started
					//and get static master drift per msec (nsec unit)
					if (ERROR_SUCCESS == Ecat64ReadDcLocalTime())
					if (ERROR_SUCCESS == Ecat64CompDcOffset())
					if (ERROR_SUCCESS == Ecat64CompDcPropDelay())
					if (ERROR_SUCCESS == Ecat64CompDcDrift(&CompLoops))
					if (ERROR_SUCCESS == Ecat64DcControl())
					{
						//Init process telegrams (required for AL_STATE_SAFE_OP)
						Ecat64InitProcessTelegrams();

						//********************************************************
						//Start cyclic operation
						//********************************************************
						if (ERROR_SUCCESS == Ecat64CyclicStateControl(&__StateObject, AL_STATE_SAFE_OP)) { Sleep(500);
						if (ERROR_SUCCESS == Ecat64CyclicStateControl(&__StateObject, AL_STATE_OP))      { Sleep(100);

								//Set Logic Ready Flag
								__bLogicReady = TRUE;

								//Do a check loop
								printf("\nPress any key stop EtherCat ...\n");
								printf("Stations Number %d\n", __StationNum);

								while (!kbhit()) {
									doRun_ProfileVelocity();

									//Do some delay
									Sleep(100);
								}

								//Reset Logic Ready Flag
								__bLogicReady = FALSE;
								Sleep(100);
		
								//********************************************************
								//Stop cyclic operation
								//********************************************************
								Ecat64CyclicStateControl(&__StateObject, AL_STATE_INIT);
							}
						}
					}
				}
			}
		}
		//Destroy ECAT core
		Sha64EcatDestroy(&EcatParams);
	}

	//Check Status
	if		(EcatParams.EthParams.pUserStack == NULL)	{ printf("\n*** Ethernet Stack Failed ***\n"); }
	else if (EcatParams.EthParams.err_cnts.Phy != 0)	{ printf("\n*** No Link ***\n"); }
	else if (EcatParams.pUserList == NULL)				{ printf("\n*** No Station List ***\n"); }
	else if (EcatParams.StationNum == 0)				{ printf("\n*** No Station Found ***\n"); }
	else												{ printf("\n*** OK ***\n"); }

	//Wait for key press
	printf("\nPress any key ...\n"); 
	while (!kbhit()) { Sleep(100); }

//******************
#ifdef SEQ_DEBUG
	SEQ_DETACH(); //Detach from sequence memory
#endif
//******************
}
