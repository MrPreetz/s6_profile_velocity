// *****************************************************************
// DEFINES - Pool for all global constant 
// 
// DISCLAIMER:
// All programs in this release are provided "AS IS, WHERE IS", WITHOUT ANY WARRENTIES, EXPRESS OR IMPLIED.
// There may be technical or editorial omissions in the programs and their specifications.
// These programs are provided solely dor user appliucation development and user assumes all responsibility for their use.
// Programsand their content are subject to change without notice
// 
// Version	Date		Who			What
// *****************************************************************
// V 1.001	28.07.21	MRupf Oxni	Adaption for Profile Velocity and S6
// V 1.000	25.09.20	MRupf Oxni	First version
// *****************************************************************
#pragma once
#include <stdint.h>

// SW Version
static const uint32_t SW_VERSION_DEFINES = 0x1001;

// Constants Real-Time
const uint32_t REALTIME_PERIOD = 2000;	// Realtime sampling period [usec]
const uint32_t SYNC_CYCLES = 1;			// EtherCat Cycle time 2ms = 2 * 1000usec
const uint32_t CYCLE_TIME_IN_MS = 0x00000002;

// Constants S6
const double POS_SCALE_DRIVE_S6 = 1048576.0;		// co03
const double VEL_SCALE_DRIVE_S6 = 16.0;				// co02
const double CUR_SCALE_DRIVE_S6 = 100.0;			// ru10 fix resolution of x.00 Arms
const int32_t VEL_CMD_SCALE_DRIVE_S6 = 8192;		// vl.21 fix resolution of 1/8192rpm

const uint16_t BIT_MASK_DRIVE_FAULT = 0x0008;		// Bit 3
const uint16_t BIT_MASK_DRIVE_WARNING = 0x0080;		// Bit 7
const uint16_t BIT_MASK_DRIVE_STO_S6 = 0xC000;		// Bit 14+15
const uint16_t BIT_MASK_DRIVE_STATUS = 0x007F;		// Bit 0-6
const uint16_t BIT_MASK_DRIVE_CMD = 0x000F;			// Bit 0-3
const uint16_t BIT_MASK_DRIVE_RESET = 0x0080;		// Bit 7
const uint16_t BIT_MASK_DRIVE_STOP = 0x0100;		// Bit 8



