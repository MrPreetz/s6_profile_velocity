#include "S6_PV.h"
#include "DEFINES.h"

// Constructor
S6ProfileVelocity::S6ProfileVelocity(double posScaling)
	:txMap(nullptr)
	, rxMap(nullptr)
	, _posScaling(posScaling)
{
}

// Return drive fault
bool S6ProfileVelocity::getDriveFault() {
	return txMap != nullptr ? txMap->statusWord & BIT_MASK_DRIVE_FAULT : false;
}

// Return drive warning
bool S6ProfileVelocity::getDriveWarning() {
	return txMap != nullptr ? txMap->statusWord & BIT_MASK_DRIVE_WARNING : false;
}

// Return error status
bool S6ProfileVelocity::getErrorStatus() {
	return _error;
}

// Return enable status
bool S6ProfileVelocity::getEnableStatus() {
	return _isEnabled;
}

// Return status as bit combined int
uint32_t S6ProfileVelocity::getDriveStatus()
{
	uint32_t _temp = (int)_isEnabled * 2;	// Bit 1, Enabled
	_temp |= _error * 32; // Bit 5, Error

	// Only read the bus bits if the pointer is valid
	if (txMap != nullptr) {
		_temp |= (bool)(txMap->statusWord & 8) * 16; // Bit 4, Fault (Bit3 in 6041)
		_temp |= (bool)(txMap->statusWord & 128) * 64; // Bit 6, Warning (Bit7 in 6041)
		_temp |= (bool)(txMap->digitalInput & BIT_MASK_DRIVE_STO_S6) * 128; // Bit 7, STO
	}
	return _temp;
}

// Get Actual Position as float in User Units
double S6ProfileVelocity::getActPosUU() {
	return txMap != nullptr ? (double)((_internalPos + (_internalPosOverflow * ULONG_MAX)) / POS_SCALE_DRIVE_S6) * _posScaling : 0.0;
}

// Get Actual Speed in RPM
double S6ProfileVelocity::getActVel() {
	return txMap != nullptr ? ((double)txMap->actVel) / VEL_SCALE_DRIVE_S6 : 0.0;
}

// Get Actual Current
double S6ProfileVelocity::getActCurrent() {
	return txMap != nullptr ? ((double)txMap->actCur) / CUR_SCALE_DRIVE_S6 : 0.0;
}

// Get Actual Status of the digital inputs
uint32_t S6ProfileVelocity::getDigInStat() {
	return txMap != nullptr ? ((uint32_t)txMap->digitalInput) : 0;
}

// REALTIME TASK this routine must be called in each cycle
// Mapping of all commands to and from the EtherCat
void S6ProfileVelocity::realTimeTask(const TX_MAP_S6A_v1* TxMap, RX_MAP_S6A_v1* RxMap) {
	txMap = TxMap;
	rxMap = RxMap;
	uint16_t cmdWord = 0;
	uint16_t statusWord = 0;

	if (txMap != nullptr) {
		statusWord = (txMap->statusWord & BIT_MASK_DRIVE_STATUS);
		if (_enable) {
			switch (statusWord)
			{
				// Switch on disabled
			case 0x50:
			case 0x70:
				cmdWord = 0x06;
				break;
				// Ready to switch on
			case 0x31:
				cmdWord = 0x07;
				break;
				// Switched On
			case 0x33:
				cmdWord = 0x0F;
				break;
				// Operation enabled
			case 0x37:
				_isEnabled = true;
				cmdWord = 0x0F;
				break;
			}
		} // Else disable
		else {
			_isEnabled = false;
			_moveJog = false;
			cmdWord = 0;
		}

		// Include Reset
		if (_doReset) {
			cmdWord = cmdWord | BIT_MASK_DRIVE_RESET;
			_doReset = false;
		}

		// Set profile velocity mode as default
		if (txMap->opMode != 2) {
			rxMap->opMode = 2;
		}

		// Read Position and include overflow (upscaling to 64-Bit from 32-Bit)
		if (_internalPos != 0) {
			if (((int64_t)_internalPos - txMap->actPos) > (1073741824)) {
				// + Position overflow
				_internalPosOverflow++;
			}
			else if ((int64_t)(_internalPos - txMap->actPos) < (-1073741824)) {
				// - Position overflow
				_internalPosOverflow--;
			}
		}
		_internalPos = txMap->actPos;

		// Read active mode
		_actMode = txMap->opMode;
		// Map output data
		rxMap->cmdVelPV = _cmdVel;
		rxMap->cmdVelPV2 = _cmdVelHiRes;
		rxMap->controlWord = cmdWord;	// DS402.CONTROLWORD
		rxMap->digitalOutput = _digitalOutput;
	}
}

// Enable/Disable procedure
void S6ProfileVelocity::enableDrive(bool enable) {
	_enable = enable && !getDriveFault();
}

// Reset drive faults
void S6ProfileVelocity::resetFault() {
	_doReset = true;
	_error = false;
}

// Jog inputs scaled in UU
void S6ProfileVelocity::moveVel(double moveVel) {
	_cmdVel = (int32_t)(moveVel);
	_cmdVelHiRes = (int32_t)((moveVel - _cmdVel) * VEL_CMD_SCALE_DRIVE_S6);
}

// Set digital output (outIdx 1 or 2)
void S6ProfileVelocity::setDigOut(int outIdx, bool value) {
	if (value) {
		_digitalOutput |= outIdx;	// Set output true
	}
	else {
		_digitalOutput &= ~outIdx;	// Set output false
	}
}

// SDO control to the drive
uint64_t S6ProfileVelocity::sendSDOCmd(PSTATION_INFO pStation, bool iWrite, USHORT idx, UCHAR subIdx, ULONG length, PUCHAR data) {
	if (iWrite) {
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, idx, subIdx, length, data)) return -1;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, idx, subIdx, length, data)) return -2;
		return 1;
	}
	else {
		ULONG responesLen = 0;
		uint8_t responseData[MAX_ECAT_DATA] = { 0 };
		uint64_t outData2 = 0;
		if (ERROR_SUCCESS != Ecat64SdoInitUploadReq(pStation, idx, subIdx)) return -1;
		if (ERROR_SUCCESS != Ecat64SdoInitUploadResp(pStation, &responesLen, responseData)) return -2;
		// Byte add the result
		for (size_t i = 0; i < responesLen; i++) {
			outData2 += (uint64_t)responseData[i] << (i * 8);
		}
		// Get the correct sign for 32-Bit variables
		if (responesLen == 4) {
			outData2 = (uint64_t)outData2;
		}
		return outData2;
	}
}

// SDO download request
uint64_t S6ProfileVelocity::sdoControl_S6(PSTATION_INFO pStation) {
	uint32_t sdoData = 0;
	UINT64 sdoWriteNo = 0;

	switch (sdoWriteNo)
	{
	case 0:
		sdoData = 0x00000000;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c13, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 1:
		sdoData = 0x00001a00;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c13, 0x01, 2, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 2:
		sdoData = 0x00000001;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c13, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 3:
		sdoData = 0x00000000;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a00, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 4:
		sdoData = 0x60410010;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a00, 0x01, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 5:
		sdoData = 0x60640020;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a00, 0x02, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 6:
		sdoData = 0x606c0020;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a00, 0x03, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 7:
		sdoData = 0x60610008;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a00, 0x04, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 8:
		sdoData = 0x60440010;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a00, 0x05, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 9:
		sdoData = 0x2c0a0020;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a00, 0x06, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 10:
		sdoData = 0x00000006;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1a00, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 11:
		sdoData = 0x00000000;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c12, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 12:
		sdoData = 0x00001600;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c12, 0x01, 2, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 13:
		sdoData = 0x00000001;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1c12, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 14:
		sdoData = 0x00000000;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1600, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 15:
		sdoData = 0x60400010;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1600, 0x01, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 16:
		sdoData = 0x60600008;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1600, 0x02, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 17:
		sdoData = 0x60420010;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1600, 0x03, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 18:
		sdoData = 0x23150020;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1600, 0x04, 4, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 19:
		sdoData = 0x00000004;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadReq(pStation, 0x1600, 0x00, 1, (PUCHAR)&sdoData)) break;
		if (ERROR_SUCCESS != Ecat64SdoInitDownloadResp(pStation)) break;
		sdoWriteNo++;
	case 20:	
		//*** Disable SDO automation ***
		pStation->SdoNum = 0;
		return -1;
		}

	//*** Something failed ***
	return sdoWriteNo;
}